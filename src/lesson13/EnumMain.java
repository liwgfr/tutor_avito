package lesson13;

import java.util.Arrays;
import java.util.Scanner;

// Enum - перечисление (специальный тип класса)
// Enum представляет собой набор константных значений, которые могут содержать в конструкторе вспомогательную информацию
// Все константы Enum'a неизменяемы
public class EnumMain {
    // Дается номер месяца - вывести его название и наоборот
    public static void main(String[] args) {
//        System.out.println(Months.January); // toString()
//        // Инициализировать Enum нельзя (!)
//        // основные методы у констант:
//        System.out.println(Months.June.name()); // Возвращает название константы
//        System.out.println(Months.June.ordinal()); // Возвращает порядок (индекс) константы
//
//        System.out.println(Months.valueOf("January").getSeason()); // Позволяет из строки получить enum, если по нашей строке НЕТ enum -> исключение
//        System.out.println(Arrays.toString(Months.values())); // возвращает массив enum Months[]
//
//        // Setter пример
//        System.out.println(Months.May.getSeason());
//        Months.May.setSeason("Блабла");
//        System.out.println(Months.May.getSeason());

        // Task
        Scanner in = new Scanner(System.in);
        int numberOfMonth = in.nextInt();
        for (int i = 0; i < Months.values().length; i++) {
            if (Months.values()[i].getNumberOfMonth() == numberOfMonth) {
                System.out.println(Months.values()[i].name());
            }
        }
        String nameOfMonth = in.next();
        System.out.println(Months.valueOf(nameOfMonth.toUpperCase()).getSeason());

    }
}

enum Months {
    // Сначала (!) обязательно идут перечисления
    JANUARY(1, "Зима"), // передаем в конструктор
    FEBRUARY(2, "Зима"),
    MARCH(3, "Весна"),
    APRIL(4, "Весна"),
    MAY(5, "Весна"),
    JUNE(6, "Лето"),
    JULY(7, "Лето"),
    AUGUST(8, "Лето"),
    OCTOBER(10, "Осень"),
    SEPTEMBER(9, "Осень"),
    NOVEMBER(11, "Осень"),
    DECEMBER(12, "Зима");

    private int numberOfMonth;
    private String season;

    Months(int numberOfMonth, String season) {
        this.numberOfMonth = numberOfMonth;
        this.season = season;
    }

    public int getNumberOfMonth() {
        return numberOfMonth;
    }

    public void setNumberOfMonth(int numberOfMonth) {
        this.numberOfMonth = numberOfMonth;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }
    //    @Override
//    public String toString() { // Не рекомендуется переопределять
//        // Но если такая нужда возникла - тогда следим, чтобы в нужных местах мы вызывали .name(), а не toString();
//        return "I am enum";
//    }
}
