package lesson13;

public class Main {
    public static void main(String[] args) {
        Parent p = new Parent("Sergey");
        Child c = new Child( 10);
        System.out.println(p.name);
        System.out.println(c.age);
        System.out.println(c.name);
    }
}
class Parent {
   public String name;

    public Parent(String name) { // EntryPoint
        this.name = name;
    }
    public Parent() {}
}
// Для использования класса - мы начинаем работу через конструктор
// Конструктор - входная точка для работы с классом (полями, методами)
class Child extends Parent {
    public int age;
    public Child(int age) {
        super();
        super.name = "Default";
        this.age = age;
        // позволяет вызвать конструктор родителя (зайти во входную точку)
    } // Мы не можем использовать дочерний класс, если нет EntryPoint в родительский


}
