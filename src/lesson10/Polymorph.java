package lesson10;

public class Polymorph {
    // Перегрузка и переопределение
    public static void main(String[] args) {
        Person p = new Person();
        System.out.println(p.calcSum('1', '8'));
        System.out.println(p.calcSum(2, 3));
        System.out.println(p.calcSum(2, 1, 5));
        p.hello();
        Student st = new Student();
        st.hello();
        Employee e = new Employee();
        e.hello();
        // В консоли пример полиморфизма
        // Полиморфизм гласит, что у одного метода может быть несколько реализаций в зависимости от выбранного класса (в рамках наследования)
    }
}

class Person {
    // Перегрузка -> это когда сигнатура метода полностью совпадает, но (!) параметры метода либо различаются по кол-ву, либо по типу принимаемых данных
    // Пример перегрузки:
    public int calcSum(int x, int y) {
        return x + y;
    }
    public int calcSum(int x, int y, int z) {
        return x + y + z;
    }
    public int calcSum(char x, char y) {
        return x - '0' + y - '0';
    }
    // ---------------
    // Наследование [x] Инкапсуляция [x] Абстракция Полиморфизм [x]
    // Переопределение -> это изменение ИЛИ создание поведения определенного метода в рамках конкретного класса. Если мы говорим об изменении кода метода -> то это возможно тогда и только тогда,
    // когда текущий класс унаследован от какого-либо другого класса
    // Создание поведения метода возможно тогда, когда мы имплементировали интерфейс ИЛИ унаследовались от абстрактного класса (причем в них реализации данного метода нет)

    public void hello() {
        System.out.println("I am Person!");
    }
}

class Student extends Person {

    @Override // Аннотация - это специальная мета-информация
    // 1) Своеобразная пометка для программистов
    // 2) Своеобразная пометка для JVM
    public void hello() {
        System.out.println("I am Student!");
    }
}

class Employee extends Person {
    @Override
    public void hello() {
        System.out.println("I am Employee!");
    }
}
