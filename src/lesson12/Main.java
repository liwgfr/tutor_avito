package lesson12;

public class Main {
    // Интерфейс в Java - максимальная степень абстракции
    // Интерфейсы не содержат конструкторов
    // Все поля интерфейсов публичные статичные и неизменяемые
    // Объявить (проинициализировать) интерфейс невозможно Interface i = new Interface();
    // Java <8 -> в интерфейсах не могло быть реализации (все методы abstract) (нет ключевого слова default)
    // Java >=8 -> такая возможность появилась с помощью ключевого слова default
    public static void main(String[] args) {
        Database database = new MongoDB("Sergey", "2021");
        // Так, как выше можно писать только если правая часть implements интерфейс (!)
        // С Абстрактными классами -> только если правая часть extends от Abstract class
        database.connectToDatabase();
        database.saveToDatabase(32);
        System.out.println(database.getFromDatabase());

        database = new MySQL("Sergey", "2022");
        System.out.println();
        database.connectToDatabase();
        database.saveToDatabase(52);
        System.out.println(database.getFromDatabase());
    }
}

interface Database {
    String login = "Sergey"; // static public и final (неизменяемое)

    default void connectToDatabase() { // обычный метод: public void connectToDatabase() { //code }
        System.out.println("I am not connected");
    }

    void saveToDatabase(int value); // abstract
    int getFromDatabase();

    // Зачем тогда абстрактные классы или интерфейсы? Почему не оставить что-то одно?
    // Java >= 8
    // 1) В интерфейсах нет конструкторов (то есть, нельзя создать объект интерфейса)
    // 2) Все поля в интерфейсах public, static final (публичные, статичные и неизменяемые)
    // 3) Проблема обратной совместимости (с каждой новой версией - добавляем новые фичи, но при этом поддерживаем возможность запуска старых версий)
}

class MongoDB implements Database {
    private String login;
    private String password;
    private int savedValue;

    public MongoDB(String login, String password) {
        this.login = login;
        this.password = password;
    }

//    @Override
//    public void connectToDatabase() {
//        System.out.println("Connected to MongoDB");
//    }

    @Override
    public void saveToDatabase(int value) {
        this.savedValue = value;
    }

    @Override
    public int getFromDatabase() {
        return savedValue;
    }
}

class MySQL implements Database {
    private String login;
    private String password;
    private int savedValue;

    public MySQL(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public void connectToDatabase() {
        System.out.println("Connected to MySQL");
    }

    @Override
    public void saveToDatabase(int value) {
        this.savedValue = value;
    }

    @Override
    public int getFromDatabase() {
        return savedValue;
    }
}
