import java.math.BigInteger;
import java.util.Scanner;

public class Q1 {
    public static void main(String[] args) {
        //расширенна версия с использованием больших значений в том числе 
        Scanner in = new Scanner(System.in);
        String num = in.nextLine();
        String[] arr = num.split("");
        BigInteger fact = new BigInteger("1");
        // 5! -> 1 * 2 * 3 * 4 * 5 -> 120
        // (n-1)*n
        // 5! -> 4! * 5 -> 3! * 4 * 5 -> ...
        // 3! -> 1 * 2 * 3
        // 3! <=> 2! * 3 <=> 1! * 2 * 3
        // 0! -> 1
        for (int i = 0; i < arr.length; i++) {
            if (Integer.parseInt(arr[i]) == 0) {
                arr[i] = "1";
            }
            BigInteger big1 = new BigInteger(arr[i]);
            fact = fact.multiply(big1);
        }
        System.out.println(fact);
    }
}

class Acmp43 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        //Получаем строку
        String sequence = in.nextLine();
        if (sequence.length() > 100) {
            System.out.println("Количесто цифр превышает допустимое значение");
        }
        //готовим под нее массив char и вспомогательные счетчики
        char[] arr_num = new char[sequence.length()];
        int count = 0;
        int helper = 0;
        //перебираем в массив char каждый элемент строки
        for (int i = 0; i < sequence.length(); i++) {
            arr_num[i] = sequence.charAt(i);
        }
        //считаем последовательность нулей
        for (int y = 0; y < arr_num.length; y++) {
            if (arr_num[y] == '0') {
                helper++;
            } else {
                helper = 0;
            }
            //Пока идут нули, helper величивается и если это число больше чем count, то count принимает
            //значение helper. Как только "натыкаемся" на другую цифру, значит последовательность нулей обрывается
            //и счетчик нулей обнуляется.
            if (helper > count) {
                count = helper;
            }
        }
        System.out.println("The longest \"0\" sequence is: " + count);
    }
}